FROM ubuntu:22.04
LABEL Description="Build Environment"

ENV HOME /root
ENV PROJECT_DIR /project

SHELL ["/bin/bash", "-c"]

# Install dependencies
RUN apt update && apt -y --no-install-recommends install \
    build-essential \
    clang \
    cmake \
    gdb \
    wget \
    autoconf \
    automake \
    libtool \
    pkg-config \ 
    git \
    ca-certificates

# Install spdlog
RUN apt -y install libspdlog-dev

# Install gRPC/Protoc

# Update root certificates
RUN update-ca-certificates

# Install protocol buffers
#RUN cd ${HOME} && \
#    apt install -y protobuf-compiler libprotobuf-dev


# Set path and local variables
ENV CC="/usr/bin/clang"
ENV CXX="/usr/bin/clang++"

# Install gRPC

RUN cd ${HOME} && git clone --recurse-submodules \
    -b v1.51.0 --depth 1 --shallow-submodules \ 
    https://github.com/grpc/grpc
RUN git config --global http.sslverify false
RUN cd ${HOME}/grpc && \
    mkdir -p cmake/build &&\
    pushd cmake/build && \
    cmake -DgRPC_INSTALL=ON \
        -DgRPC_BUILD_TESTS=OFF \
        ../.. && \
    make -j16 && \
    make install &&\
    popd

RUN cd ${HOME} && git clone --recurse-submodules \
    https://github.com/catchorg/Catch2.git && \
    cd ${HOME}/Catch2 && \
    cmake -Bbuild -H. -DBUILD_TESTING=OFF && \
    cmake --build build/ --target install -j16
