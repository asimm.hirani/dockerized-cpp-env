cmake_minimum_required(VERSION 3.20)

set(PROJECT_NAME TestProject)

project(${PROJECT_NAME})

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} \
                    -Wall \
                    -Wextra \
                    -Werror \
                    -fsanitize=undefined")


# Add spdlog
find_package(spdlog REQUIRED)

# Add pbuffers
find_package(Protobuf REQUIRED)
include_directories(${Protobuf_INCLUDE_DIRS})

# Add catch2
find_package(Catch2 3 REQUIRED)

# Add gRPC
find_package(Protobuf REQUIRED)
find_package(gRPC CONFIG REQUIRED)

add_subdirectory(src)

